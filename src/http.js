import Axios from 'axios'

const http = Axios.create({
  baseURL: '/api'
})

http.FILE_MANAGER_UPLOAD = 'file-manager/upload'
http.FILE_MANAGER_UPLOAD_MEDIA = 'file-manager/upload-media'
http.FILE_MANAGER_DELETE = 'file-manager/delete/'

export default http

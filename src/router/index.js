import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Videos from '../components/Videos'
import Posts from '../components/Posts'
import Sections from '../components/Sections'
import Events from '../components/Events'
import Quotations from '../components/Quotations'
import Books from '../components/Books'
import Teachers from '../components/Teachers'
import Calendar from '../components/Calendar'
import Quotes from '../components/Quotes'
import UsefulLinks from '../components/UsefulLinks'
import Partners from '../components/Partners'
import Settings from '../components/Settings'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', component: Home },
    { path: '/videos', component: Videos },
    { path: '/posts', component: Posts },
    { path: '/sections', component: Sections },
    { path: '/events', component: Events },
    { path: '/quotations', component: Quotations },
    { path: '/books', component: Books },
    { path: '/teachers', component: Teachers },
    { path: '/calendar', component: Calendar },
    { path: '/quotes', component: Quotes },
    { path: '/useful-links', component: UsefulLinks },
    { path: '/partners', component: Partners },
    { path: '/settings', component: Settings }
  ]
})
